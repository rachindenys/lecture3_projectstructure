﻿using Lecture3_DAL.Entities;
using Lecture3_DAL.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lecture3_DAL
{
    public class UnitOfWork : IDisposable
    {
        private IRepository<Project> _projectRepository;
        private IRepository<Task> _taskRepository;
        private IRepository<Team> _teamRepository;
        private IRepository<User> _userRepository;

        public UnitOfWork
            (
                IRepository<Project> projectRepository,
                IRepository<Task> taskRepository,
                IRepository<Team> teamRepository,
                IRepository<User> userRepository
            )
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public IRepository<Project> Projects { get { return _projectRepository; } }
        public IRepository<Task> Tasks { get { return _taskRepository; } }
        public IRepository<Team> Teams { get { return _teamRepository; } }
        public IRepository<User> Users { get { return _userRepository; } }

        public void LoadTestData()
        {
            using (StreamReader r = new StreamReader($@"../Lecture3_DAL/DataStorage/projects.json"))
            {
                string json = r.ReadToEnd();
                var content = JsonConvert.DeserializeObject<List<Project>>(json);
                foreach(var item in content)
                {
                    _projectRepository.Create(item);
                }
            }
            using (StreamReader r = new StreamReader($@"../Lecture3_DAL/DataStorage/tasks.json"))
            {
                string json = r.ReadToEnd();
                var content = JsonConvert.DeserializeObject<List<Task>>(json);
                foreach (var item in content)
                {
                    _taskRepository.Create(item);
                }
            }
            
            using (StreamReader r = new StreamReader($@"../Lecture3_DAL/DataStorage/users.json"))
            {
                string json = r.ReadToEnd();
                var content = JsonConvert.DeserializeObject<List<User>>(json);
                foreach (var item in content)
                {
                    _userRepository.Create(item);
                }
            }
            
            using (StreamReader r = new StreamReader($@"../Lecture3_DAL/DataStorage/teams.json"))
            {
                string json = r.ReadToEnd();
                var content = JsonConvert.DeserializeObject<List<Team>>(json);
                foreach (var item in content)
                {
                    _teamRepository.Create(item);
                }
            }
            SaveChanges();
        }

        public void SaveChanges()
        {
            var projects = _projectRepository.Get();
            var tasks = _taskRepository.Get();
            var teams = _teamRepository.Get();
            var users = _userRepository.Get();
            var connectedTeams = teams
                .GroupJoin(users,
                t => t.Id,
                u => u.TeamId,
                (t, u) =>
                {
                    return new Team
                    {
                        Id = t.Id,
                        Name = t.Name,
                        CreatedAt = t.CreatedAt,
                        Users = u.ToList()
                    };
                });
            var connectedTasks = tasks
                .Join(users,
                t => t.PerformerId,
                u => u.Id,
                (t, u) =>
                {
                    return new Task
                    {
                        Id = t.Id,
                        Name = t.Name,
                        PerformerId = t.PerformerId,
                        ProjectId = t.ProjectId,
                        State = t.State,
                        CreatedAt = t.CreatedAt,
                        FinishedAt = t.FinishedAt,
                        Description = t.Description,
                        Performer = u
                    };
                });
            var connectedProjects = projects
                .Join(users,
                p => p.AuthorId,
                u => u.Id,
                (p, u) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        CreatedAt = p.CreatedAt,
                        Author = u
                    };
                })
                .Join(connectedTeams,
                p => p.TeamId,
                t => t.Id,
                (p, t) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        CreatedAt = p.CreatedAt,
                        Team = t
                    };
                })
                .GroupJoin(connectedTasks,
                p => p.Id,
                t => t.ProjectId,
                (p, t) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        Team = p.Team,
                        CreatedAt = p.CreatedAt,
                        Tasks = t.ToList()
                    };
                });
            var connectedUsers = users
                .Join(teams,
                u => u.TeamId,
                t => t.Id,
                (u, t) =>
                {
                    return new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        Id = u.Id,
                        LastName = u.LastName,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = t
                    };
                })
                .Union(users.Where(u => u.TeamId == null))
                .GroupJoin(tasks,
                u => u.Id,
                t => t.PerformerId,
                (u, t) =>
                {
                    return new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        Id = u.Id,
                        LastName = u.LastName,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = u.Team,
                        Tasks = t.ToList()
                    };
                });
            _projectRepository.RefreshData(connectedProjects);
            _taskRepository.RefreshData(connectedTasks);
            _teamRepository.RefreshData(connectedTeams);
            _userRepository.RefreshData(connectedUsers);
        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
