﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture3_DAL.Entities
{
    public class Project
    {
        public int Id { get; set; }
        [NotMapped]
        public User Author { get; set; }
        public int AuthorId { get; set; }
        [NotMapped]
        public Team Team { get; set; }
        public int? TeamId { get; set; }
        [NotMapped]
        public List<Task> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
