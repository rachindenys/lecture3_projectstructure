﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture3_DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        [NotMapped]
        public Team Team { get; set; }
        [NotMapped]
        public List<Task> Tasks { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
