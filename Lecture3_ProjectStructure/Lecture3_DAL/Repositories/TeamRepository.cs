﻿using Lecture3_DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lecture3_DAL.Repositories
{
    public sealed class TeamRepository : IRepository<Team>
    {
        private List<Team> teams = new List<Team>();
  
        public void RefreshData(IEnumerable<Team> data)
        {
            if (data.Count() != 0)
                teams = data.ToList();
        }
        public void Create(Team item)
        {
            teams.Add(item);
        }

        public void Delete(int id)
        {
            teams.Remove(teams.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<Team> Get()
        {
            return teams;
        }

        public Team Get(int id)
        {
            return teams.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, Team item)
        {
            var itemId = teams.IndexOf(teams.FirstOrDefault(p => p.Id == id));
            teams[itemId] = item;
            teams[itemId].Id = id;
        }
    }
}
