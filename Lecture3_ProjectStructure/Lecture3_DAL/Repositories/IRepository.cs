﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_DAL.Repositories
{
    public interface IRepository<T> where T : class
    {
        void RefreshData(IEnumerable<T> data);
        IEnumerable<T> Get();
        T Get(int id);
        void Create(T item);
        void Update(int id, T item);
        void Delete(int id);
    }
}
