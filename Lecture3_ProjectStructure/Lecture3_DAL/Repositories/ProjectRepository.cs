﻿using Lecture3_DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;  // Do delete
using System.Linq;
using System.Text;

namespace Lecture3_DAL.Repositories
{
    public sealed class ProjectRepository : IRepository<Project>
    {
        private List<Project> projects = new List<Project>();


        public void RefreshData(IEnumerable<Project> data)
        {
            if (data.Count() != 0)
                projects = data.ToList();
        }
        public void Create(Project item)
        {
            projects.Add(item);
        }

        public void Delete(int id)
        {
            projects.Remove(projects.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<Project> Get()
        {
            return projects;
        }

        public Project Get(int id)
        {
            return projects.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, Project item)
        {
            var itemId = projects.IndexOf(projects.FirstOrDefault(p => p.Id == id));
            projects[itemId] = item;
            projects[itemId].Id = id;
        }
    }
}
