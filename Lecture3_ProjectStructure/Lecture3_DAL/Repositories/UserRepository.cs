﻿using Lecture3_DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lecture3_DAL.Repositories
{
    public sealed class UserRepository : IRepository<User>
    {
        private List<User> users = new List<User>();

        public void RefreshData(IEnumerable<User> data)
        {
            if (data.Count() != 0)
                users = data.ToList();
        }
        public void Create(User item)
        {
            users.Add(item);
        }

        public void Delete(int id)
        {
            users.Remove(users.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<User> Get()
        {
            return users;
        }

        public User Get(int id)
        {
            return users.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, User item)
        {
            var itemId = users.IndexOf(users.FirstOrDefault(p => p.Id == id));
            users[itemId] = item;
            users[itemId].Id = id;
        }
    }
}
