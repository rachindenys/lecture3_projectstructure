﻿using Lecture3_DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lecture3_DAL.Repositories
{
    public sealed class TaskRepository : IRepository<Task>
    {
        private List<Task> tasks = new List<Task>();
        public void RefreshData(IEnumerable<Task> data)
        {
            if (data.Count() != 0)
                tasks = data.ToList();
        }
        public void Create(Task item)
        {
            tasks.Add(item);
        }

        public void Delete(int id)
        {
            tasks.Remove(tasks.FirstOrDefault(t => t.Id == id));
        }

        public IEnumerable<Task> Get()
        {
            return tasks;
        }

        public Task Get(int id)
        {
            return tasks.FirstOrDefault(t => t.Id == id);
        }

        public void Update(int id, Task item)
        {
            var itemId = tasks.IndexOf(tasks.FirstOrDefault(t => t.Id == id));
            tasks[itemId] = item;
            tasks[itemId].Id = id;
        }
    }
}
