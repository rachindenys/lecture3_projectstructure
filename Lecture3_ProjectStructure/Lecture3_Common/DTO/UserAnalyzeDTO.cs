﻿using Lecture3_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_Common.DTO
{
    public class UserAnalyzeDTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
