﻿using Lecture3_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_Common.DTO
{
    public class ProjectAnalyzeDTO
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}
