﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture3_Common.DTO
{
    public class ProjectDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
        [JsonProperty("teamId")]
        public int? TeamId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("deadline")]
        public string Deadline { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}
