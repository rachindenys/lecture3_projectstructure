﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_Common.DTO
{
    public class SortedUserByNameAndTasksDTO
    {
        public string UserName { get; set; }
        public List<TaskDTO> SortedTasks { get; set; }
    }
}
