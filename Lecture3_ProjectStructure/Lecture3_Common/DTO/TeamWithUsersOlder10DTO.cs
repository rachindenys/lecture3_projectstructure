﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_Common.DTO
{
    public class TeamWithUsersOlder10DTO
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserDTO> UsersOlder10 { get; set; }
    }
}
