﻿using Lecture3_BLL.Services;
using Lecture3_Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture3_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(
            TeamService teamService
            )
        {
            _teamService = teamService;
        }

        [HttpPost]
        public void Create([FromBody] TeamDTO teamDTO)
        {
            _teamService.Create(teamDTO);
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _teamService.Delete(id);
        }
        [HttpGet]
        public IEnumerable<TeamDTO> Get()
        {
            return _teamService.Get();

        }
        [HttpGet("{id}")]
        public TeamDTO Get(int id)
        {
            return _teamService.Get(id);
        }

        [HttpPut("{id}")]
        public void Update(int id, [FromBody] TeamDTO teamDTO)
        {
            _teamService.Update(id, teamDTO);
        }
    }
}
