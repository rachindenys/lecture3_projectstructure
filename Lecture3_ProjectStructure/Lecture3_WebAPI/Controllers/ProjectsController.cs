﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture3_BLL.Services;
using Lecture3_Common.DTO;

namespace Lecture3_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(
            ProjectService projectService
            )
        {
            _projectService = projectService;
        }

        [HttpPost]
        public void Create([FromBody] ProjectDTO projectDTO)
        {
            _projectService.Create(projectDTO);
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectService.Delete(id);
        }
        [HttpGet]
        public IEnumerable<ProjectDTO> Get()
        {
            return _projectService.Get();

        }
        [HttpGet("{id}")]
        public ProjectDTO Get(int id)
        {
            return _projectService.Get(id);
        }

        [HttpPut("{id}")]
        public void Update(int id, [FromBody] ProjectDTO projectDTO)
        {
            _projectService.Update(id, projectDTO);
        }
    }
}
