﻿using Lecture3_BLL.Services;
using Lecture3_Common.DTO;
using Lecture3_DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture3_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(
            LinqService linqService
            )
        {
            _linqService = linqService;
        }
        [HttpGet("1/{id}")]
        public IDictionary<int, int> CountTasksOfCurrentUser(int id)
        {
            return _linqService.CountTasksOfCurrentUser(id);
        }
        [HttpGet("2/{id}")]
        public List<TaskDTO> GetTasksOfCurrentUser(int id)
        {
            return _linqService.GetTasksOfCurrentUser(id);
        }
        [HttpGet("3/{id}")]
        public List<Tuple<int, string>> GetFinishedThisYearTasksOfCurrentUser(int id, int year = 2019)
        {
            return _linqService.GetFinishedThisYearTasksOfCurrentUser(id);
        }
        [HttpGet("4")]
        public IEnumerable<TeamWithUsersOlder10DTO> GetTeamsWithUsersOlderThanTen()
        {
            return _linqService.GetTeamsWithUsersOlderThanTen();
        }
        [HttpGet("5")]
        public IOrderedEnumerable<SortedUserByNameAndTasksDTO> GetSortedUserByAscendingAndTasksByDescending()
        {
            return _linqService.GetSortedUserByAscendingAndTasksByDescending();
        }
        [HttpGet("6/{id}")]
        public UserAnalyzeDTO AnalyzeUserProjectsAndTasks(int id)
        {
            return _linqService.AnalyzeUserProjectsAndTasks(id);
        }
        [HttpGet("7/{id}")]
        public ProjectAnalyzeDTO AnalyzeProjectTasksAndTeam(int id)
        {
            return _linqService.AnalyzeProjectTasksAndTeam(id);
        }
        [HttpPost("all")]
        public void LoadTestData()
        {
            _linqService.LoadTestData();
        }
    }
}
