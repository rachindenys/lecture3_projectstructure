﻿using Lecture3_BLL.Services;
using Lecture3_Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture3_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(
            TaskService taskService
            )
        {
            _taskService = taskService;
        }

        [HttpPost]
        public void Create([FromBody] TaskDTO taskDTO)
        {
            _taskService.Create(taskDTO);
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _taskService.Delete(id);
        }
        [HttpGet]
        public IEnumerable<TaskDTO> Get()
        {
            return _taskService.Get();

        }
        [HttpGet("{id}")]
        public TaskDTO Get(int id)
        {
            return _taskService.Get(id);
        }

        [HttpPut("{id}")]
        public void Update(int id, [FromBody] TaskDTO taskDTO)
        {
            _taskService.Update(id, taskDTO);
        }
    }
}
