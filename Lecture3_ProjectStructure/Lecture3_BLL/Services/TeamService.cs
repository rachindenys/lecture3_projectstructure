﻿using AutoMapper;
using Lecture3_BLL.Services.Abstract;
using Lecture3_Common.DTO;
using Lecture3_DAL;
using Lecture3_DAL.Entities;
using Lecture3_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_BLL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _unitOfWork.Teams.Create(team);
            _unitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> Get()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_unitOfWork.Teams.Get());
        }

        public TeamDTO Get(int id)
        {
            return _mapper.Map<TeamDTO>(_unitOfWork.Teams.Get(id));
        }

        public void Update(int id, TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _unitOfWork.Teams.Update(id, team);
            _unitOfWork.SaveChanges();
        }
    }
}
