﻿using AutoMapper;
using Lecture3_DAL;
using Lecture3_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_BLL.Services.Abstract
{
    public abstract class BaseService<T> where T : class
    {
        private protected readonly UnitOfWork _unitOfWork;
        private protected readonly IMapper _mapper;

        public BaseService(UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
    }
}
