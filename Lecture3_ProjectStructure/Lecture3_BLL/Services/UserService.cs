﻿using AutoMapper;
using Lecture3_BLL.Services.Abstract;
using Lecture3_Common.DTO;
using Lecture3_DAL;
using Lecture3_DAL.Entities;
using Lecture3_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_BLL.Services
{
    public sealed class UserService : BaseService<User>
    {
        public UserService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            _unitOfWork.Users.Create(user);
            _unitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            _unitOfWork.Users.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> Get()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_unitOfWork.Users.Get());
        }

        public UserDTO Get(int id)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Users.Get(id));
        }

        public void Update(int id, UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            _unitOfWork.Users.Update(id, user);
            _unitOfWork.SaveChanges();
        }
    }
}
