﻿using AutoMapper;
using Lecture3_BLL.Services.Abstract;
using Lecture3_Common.DTO;
using Lecture3_DAL;
using Lecture3_DAL.Entities;
using Lecture3_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_BLL.Services
{
    public sealed class TaskService : BaseService<Task>
    {
        public TaskService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(TaskDTO taskDTO)
        {
            var task = _mapper.Map<Task>(taskDTO);
            _unitOfWork.Tasks.Create(task);
            _unitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> Get()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_unitOfWork.Tasks.Get());
        }

        public TaskDTO Get(int id)
        {
            return _mapper.Map<TaskDTO>(_unitOfWork.Tasks.Get(id));
        }

        public void Update(int id, TaskDTO taskDTO)
        {
            var task = _mapper.Map<Task>(taskDTO);
            _unitOfWork.Tasks.Update(id, task);
            _unitOfWork.SaveChanges();
        }
    }
}
