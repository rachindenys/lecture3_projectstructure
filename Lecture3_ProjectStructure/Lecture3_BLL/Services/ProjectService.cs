﻿using AutoMapper;
using Lecture3_BLL.Services.Abstract;
using Lecture3_Common.DTO;
using Lecture3_DAL;
using Lecture3_DAL.Entities;
using Lecture3_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture3_BLL.Services
{
    public sealed class ProjectService : BaseService<Project>
    {
        public ProjectService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _unitOfWork.Projects.Create(project);
            _unitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> Get()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_unitOfWork.Projects.Get());
        }

        public ProjectDTO Get(int id)
        {
            return _mapper.Map<ProjectDTO>(_unitOfWork.Projects.Get(id));
        }

        public void Update(int id, ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _unitOfWork.Projects.Update(id, project);
            _unitOfWork.SaveChanges();
        }


    }
}
