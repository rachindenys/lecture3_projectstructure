﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Lecture3_Common.DTO;

namespace Lecture3_ConsoleApp.Services
{
    class TasksHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Tasks/");
        }
        public async Task<List<TaskDTO>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TaskDTO>(content);
        }
    }
}
