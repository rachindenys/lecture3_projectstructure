﻿using Lecture3_Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_ConsoleApp.Services
{
    class LinqHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public LinqHttpService()
        {
            _httpClient.BaseAddress = new Uri($"http://localhost:5000/api/Linq/");
        }
        // 1
        public async Task<IDictionary<int, int>> CountTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"1/{userId}");
            return JsonConvert.DeserializeObject<IDictionary<int, int>>(content);
        }
        // 2
        public async Task<List<TaskDTO>> GetTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"2/{userId}");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }
        // 3
        public async Task<List<Tuple<int, string>>> GetFinishedThisYearTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"3/{userId}");
            return JsonConvert.DeserializeObject<List<Tuple<int, string>>>(content);
        }
        // 4
        public async Task<IEnumerable<TeamWithUsersOlder10DTO>> GetTeamsWithUsersOlderThanTen()
        {
            var content = await _httpClient.GetStringAsync($"4");
            return JsonConvert.DeserializeObject<IEnumerable<TeamWithUsersOlder10DTO>>(content);
        }
        // 5
        public async Task<IEnumerable<SortedUserByNameAndTasksDTO>> GetSortedUserByAscendingAndTasksByDescending()
        {
            var content = await _httpClient.GetStringAsync($"5");
            return JsonConvert.DeserializeObject<IEnumerable<SortedUserByNameAndTasksDTO>>(content);
        }
        // 6
        public async Task<UserAnalyzeDTO> AnalyzeUserProjectsAndTasks(int userId)
        {
            var content = await _httpClient.GetStringAsync($"6/{userId}");
            return JsonConvert.DeserializeObject<UserAnalyzeDTO>(content);
        }
        // 7
        public async Task<ProjectAnalyzeDTO> AnalyzeProjectTasksAndTeam(int projectId)
        {
            var content = await _httpClient.GetStringAsync($"7/{projectId}");
            return JsonConvert.DeserializeObject<ProjectAnalyzeDTO>(content);
        }

        public async void LoadTestData()
        {
            var content = new StringContent("");
            await _httpClient.PostAsync("all", content);
        }
    }
}
