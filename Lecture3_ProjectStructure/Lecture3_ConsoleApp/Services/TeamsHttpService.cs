﻿using Lecture3_Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_ConsoleApp.Services
{
    class TeamsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TeamsHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Teams/");
        }
        public async Task<List<TeamDTO>> GetAllTeams()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TeamDTO>>(content);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TeamDTO>(content);
        }
    }
}
