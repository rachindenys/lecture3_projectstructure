﻿using Lecture3_Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_ConsoleApp.Services
{
    class ProjectsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public ProjectsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"http://localhost:5000/api/Projects/");
        }
        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<ProjectDTO>>(content);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<ProjectDTO>(content);
        }
    }
}
