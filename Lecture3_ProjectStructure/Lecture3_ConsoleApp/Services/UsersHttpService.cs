﻿using Lecture3_Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_ConsoleApp.Services
{
    class UsersHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public UsersHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Users/");
        }
        public async Task<List<UserDTO>> GetAllUsers()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<UserDTO>>(content);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<UserDTO>(content);
        }
    }
}
