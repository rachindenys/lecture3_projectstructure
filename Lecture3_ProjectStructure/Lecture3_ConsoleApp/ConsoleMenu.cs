﻿using Lecture3_ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture3_ConsoleApp
{
    public static class ConsoleMenu
    {
        private static LinqHttpService _linqService = new LinqHttpService();

        private static List<Option> options = new List<Option>
            {
                new Option("1. Count tasks of current user", () => CountTasksOfCurrentUser()),
                new Option("2. Get all tasks of user with name length < 45", () =>  GetTasksOfCurrentUser()),
                new Option("3. Get finished this year tasks of user", () =>  GetFinishedThisYearTasksOfCurrentUser()),
                new Option("4. Get teams with sorted by registration date users, older than 10", () =>  GetTeamsWithUsersOlderThanTen()),
                new Option("5. Sort by user first name and task name", () =>  GetSortedUserByAscendingAndTasksByDescending()),
                new Option("6. Analyze user's last project and tasks", () =>  AnalyzeUserProjectsAndTasks()),
                new Option("7. Analyze project's tasks and team", () =>  AnalyzeProjectTasksAndTeam()),
                new Option("8. Initialize default data", () =>  InitDefaultData()),
                new Option("Exit", () => Environment.Exit(0)),
            };

        public static void CountTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = _linqService.CountTasksOfCurrentUser(userID).GetAwaiter().GetResult();
            foreach(var item in result)
            {
                Console.WriteLine($"Projectid: {item.Key}, Tasks: {item.Value}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = _linqService.GetTasksOfCurrentUser(userID).GetAwaiter().GetResult();
            Console.WriteLine();
            foreach(var item in result)
            {
                Console.WriteLine($"\n\nName: {item.Name}\nDescription: {item.Description}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = _linqService.GetFinishedThisYearTasksOfCurrentUser(userID).GetAwaiter().GetResult();
            Console.WriteLine();
            foreach (var item in result)
            {
                Console.WriteLine($"\n\nTask ID: {item.Item1}\nTask Name: {item.Item2}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetTeamsWithUsersOlderThanTen()
        {
            Console.Clear();
            var result = _linqService.GetTeamsWithUsersOlderThanTen().GetAwaiter().GetResult();
            Console.WriteLine();
            foreach(var item in result)
            {
                Console.WriteLine($"\n\nTeam ID: [{item.Id}]. Team name: {item.TeamName}. Users: \n");
                foreach(var user in item.UsersOlder10)
                {
                    Console.WriteLine($"\t\tUser name: {user.FirstName} User Birth Day: {user.BirthDay} User Registration Date: {user.RegisteredAt}");
                }
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetSortedUserByAscendingAndTasksByDescending()
        {
            Console.Clear();
            var result = _linqService.GetSortedUserByAscendingAndTasksByDescending().GetAwaiter().GetResult();
            Console.WriteLine();
            foreach (var item in result)
            {
                Console.WriteLine($"\n\nUser First Name: {item.UserName}. Tasks: \n");
                foreach(var task in item.SortedTasks)
                {
                    Console.WriteLine(task.Name);
                }
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void AnalyzeUserProjectsAndTasks()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var result = _linqService.AnalyzeUserProjectsAndTasks(userID).GetAwaiter().GetResult();
            Console.WriteLine(); 
            Console.WriteLine($"\n\nUser: {result.User.FirstName} {result.User.LastName}");
            Console.WriteLine($"Last Project: {result.LastProject?.Name}");
            Console.WriteLine($"Total tasks count: {result.TotalTasksCount}");
            Console.WriteLine($"Uncompleted and canceled tasks count: {result.TotalUncompletedAndCanceledTasks}");
            Console.WriteLine($"Longest task: {result.LongestTask?.Name}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void AnalyzeProjectTasksAndTeam()
        {
            Console.Clear();
            Console.WriteLine("Write project ID: ");
            Int32.TryParse(Console.ReadLine(), out int projectID);
            var result = _linqService.AnalyzeProjectTasksAndTeam(projectID).GetAwaiter().GetResult();
            Console.WriteLine();
            Console.WriteLine($"\n\nProject: {result.Project?.Name}");
            Console.WriteLine($"Longest task by description: {result.LongestTaskByDescription?.Description}");
            Console.WriteLine($"Shortest task by name: {result.ShortestTaskByName?.Name}");
            Console.WriteLine($"Total team count where project description length > 20 or tasks count < 3: {result.TotalTeamCount}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void InitDefaultData()
        {
            _linqService.LoadTestData();
        }

        public static void Start()
        {

            try
            {
            int index = 0;
                ConsoleKeyInfo keyinfo;
                do
                {
                    WriteMenu(options, options[index]);
                    keyinfo = Console.ReadKey();

                    // Handle each key input (down arrow will write the menu again with a different selected item)
                    if (keyinfo.Key == ConsoleKey.DownArrow)
                    {
                        if (index + 1 < options.Count)
                        {
                            index++;
                            WriteMenu(options, options[index]);
                        }
                    }
                    if (keyinfo.Key == ConsoleKey.UpArrow)
                    {
                        if (index - 1 >= 0)
                        {
                            index--;
                            WriteMenu(options, options[index]);
                        }
                    }
                    // Handle different action for the option
                    if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        options[index].Selected.Invoke();
                        index = 0;
                    }
                }
                while (keyinfo.Key != ConsoleKey.X);
            }
            catch (Exception e)
            {
                Console.WriteLine($"You can't perfom this operation: {e.Message}");
                Console.ReadKey();
                return;
            }

            Console.ReadKey();
        }

        static void WriteMenu(List<Option> options, Option selectedOption)
        {
            Console.Clear();

            foreach (Option option in options)
            {
                if (option == selectedOption)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write(" ");
                }

                Console.WriteLine(option.Name);
            }
        }
        protected class Option
        {
            public string Name { get; }
            public Action Selected { get; }

            public Option(string name, Action selected)
            {
                Name = name;
                Selected = selected;
            }
        }
    }

}
